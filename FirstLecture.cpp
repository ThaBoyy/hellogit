#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{

	vector <char> v;
	vector <int> v1 = { 10, 14, 32};

	v.push_back('p');
	v.push_back('i');

	cout << v1.size();
	cout << endl;
	cout << v.size();
	cout << endl;

	for (int i = 0; i < 4; i++)
	{
		v1.push_back(8);
	}

	cout << v1.size();
	cout << endl;

	for (int i = 0; i < v1.size(); i++)
	{
		cout << v1[i] << " ";
	}

	cout << endl;
	cout << endl;
	
	string str1 = "Hello";
	string str2 = str1 + " World";

	cout << str1;
	cout << endl;
	cout << str2;
	cout << endl;

	cout << "Str1 = " << str1 << endl;
	cout << "The 4th Character Is " << str1[3] << endl;
	cout << "Str1 Has " << str1.size() << " Characters" << endl;
	
	for (char ch: str1)
	{
		cout << ch << " ";
	}

	cout << endl;
	return 0;
}
